# AbstRCT Argument Mining Dataset

The AbstRCT dataset consists of randomized controlled trials retrieved from the MEDLINE database via PubMed search. The trials are annotated with argument components and argumentative relations. It also contains the annotation of the outcomes and their effect.

For details regarding the creation of the dataset and experiments based on this data, please see the paper below.

  
# License

The AbstRCT dataset is released under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License:

https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode

# Citation

If you are using our dataset for research purposes, please cite the following
paper:

Tobias Mayer, Elena Cabrio and Serena Villata (2020) Transformer-based Argument Mining for Healthcare Applications. 
In Proceedings of the 24th European Conference on Artificial Intelligence (ECAI 2020), Santiago de Compostela, Spain.

Tobias Mayer, Santiago Marro, Elena Cabrio and Serena Villata (2021). Enhancing Evidence-Based Medicine with Natural Language Argumentative Analysis of Clinical Trials.
Artificial Intelligence in Medicine, 102098.

```
@article{mayer2021enhancing,
  title={Enhancing Evidence-Based Medicine with Natural Language Argumentative Analysis of Clinical Trials},
  author={Mayer, Tobias and Marro, Santiago and Cabrio, Elena and Villata, Serena},
  journal={Artificial Intelligence in Medicine},
  pages={102098},
  year={2021},
  publisher={Elsevier}
}
```
