The proportion of patients who experienced pain was significantly lower in the intracameral lidocaine group compared with the BSS group (multivariate OR 0.68, 95% CI 0.47 to 0.97; p=0.034).
The use of 0.5 cc of 1% intracameral lidocaine during phacoemulsification under topical anaesthesia significantly reduces pain experienced by patients.
