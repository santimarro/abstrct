The mean total costs per patient in the UAE group were significantly lower than those in the hysterectomy group ($11,626 vs $18,563; mean difference, -$6,936 [-37%], 95% CI: -$9,548, $4,281).
The 24-month cumulative cost of UAE is lower than that of hysterectomy.
The direct medical in-hospital costs were significantly lower in the UAE group: $6,688 vs $8,313 (mean difference, -$1,624 [-20%], 95% CI: -$2,605, -$586).
Direct medical out-of-hospital and direct nonmedical costs were low in both groups (mean cost difference, $156 in favor of hysterectomy).
