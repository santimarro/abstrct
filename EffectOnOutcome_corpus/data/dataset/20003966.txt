Food intake improved more rapidly after stent placement than after GJJ (GOO Scoring System score > or = 2: median 5 vs 8 days, respectively; P < .01)
but long-term relief was better after GJJ, with more patients living more days with a GOO Scoring System score of 2 or more than after stent placement (72 vs 50 days, respectively; P = .05).
When stent obstruction was not regarded as a major complication, no differences in complications were found (P = .4).
There were also no differences in median survival (stent: 56 days vs GJJ: 78 days) and quality of life.
Mean total costs of GJJ were higher compared with stent placement ($16,535 vs $11,720, respectively; P = .049 [comparing medians]).
GJJ was associated with better long-term results and is therefore the treatment of choice in patients with a life expectancy of 2 months or longer.
