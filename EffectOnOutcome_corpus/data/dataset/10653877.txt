Treatment with cisplatin-based chemotherapy provides a modest survival advantage over supportive care alone in advanced non-small-cell lung cancer (NSCLC).
Superior survival was observed with the combined paclitaxel regimens (median survival time, 9.9 months; 1-year survival rate, 38.9%) compared with etoposide plus cisplatin (median survival time, 7.6 months; 1-year survival rate, 31.8%; P =. 048).
Comparing survival for the two dose levels of paclitaxel revealed no significant difference.
With the exceptions of increased granulocytopenia on the low-dose paclitaxel regimen and increased myalgias, neurotoxicity, and, possibly, increased treatment-related cardiac events with high-dose paclitaxel, toxicity was similar across all three arms.
Quality of life (QOL) declined significantly over the 6 months.
QOL scores were not significantly different among the regimens.
