There was no difference between groups in mean change in EXIT25 score from baseline to 6-month follow-up assessment.
Mean hemoglobin levels were higher in the epoetin alfa group compared with the placebo group after 4 cycles of chemotherapy.
Epoetin alfa recipients had less of a decrease in FACT-An subscale scores from baseline to cycle 4 and improvement in FACT-An subscale scores at 6-month follow-up assessment compared with placebo.
These data suggest that epoetin alfa may have attenuated the cognitive impairment and fatigue that occurred during adjuvant breast cancer chemotherapy.
