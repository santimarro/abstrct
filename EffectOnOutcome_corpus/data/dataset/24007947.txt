Analgesia and early quality of recovery may be improved by epidural analgesia.
there was no difference in serious adverse events (p=0.19).
Epidural analgesia was associated with increased length of stay (up to 48 days compared to up to 34 days in the non-epidural group).
There was no difference in postoperative quality of life up to six months after surgery.
Epidural analgesia was associated with an increase in any, but not serious, postoperative complications and length of stay after abdominal hysterectomy.
the present data do not support a quality of life benefit with epidural analgesia.
