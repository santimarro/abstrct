In a randomized clinical trial in patients with advanced non-small-cell lung cancer (NSCLC), infusion with adenosine 5'-triphosphate (ATP) inhibited loss of body weight and quality of life.
No change in body composition over the 28-week follow-up period was found in the ATP group, whereas, per 4 weeks, the control group lost 0.6 kg of FM (P =.004), 0.5 kg of FFM (P =.02), 1.8% of arm muscle area (P =.02), and 0.6% of BCM/kg body weight (P =.054) and decreased 568 KJ/d in energy intake (P =.0001).
Appetite also remained stable in the ATP group but decreased significantly in the control group (P =.0004).
No significant differences in REE between the ATP and control groups were observed.
