Survival was statistically significantly better in the paclitaxel plus supportive care arm than in the supportive care alone arm (two-sided P =.037) (median survival = 6.8 months versus 4.8 months).
Cox multivariate analysis showed paclitaxel plus supportive care to be statistically significantly associated with improved survival (two-sided P =.048).
QOL was similar for both treatment arms,
except for the functional activity score of the Rotterdam Symptom Checklist, where QOL data statistically significantly favored the paclitaxel plus supportive care arm (two-sided P =.043).
The addition of paclitaxel to best supportive care significantly improved survival and time to disease progression compared with best supportive care in patients with advanced NSCLC and may improve some aspects of QOL.
