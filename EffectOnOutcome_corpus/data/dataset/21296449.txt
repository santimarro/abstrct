There was no difference in OS (p=0.992).
There was a significant decrease in pain scores from baseline to post cycles 2 and 3 (p=0.025 and p=0.002, respectively).
No difference was observed in hospitalization rate, blood transfusions, antibiotics administration and non-haematological toxicity.
the median survival observed is shorter than historical data and do not support further evaluation of these doses of single agent docetaxel in this population.
