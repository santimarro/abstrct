Placebo-treated patients had a mean increase in HgB of 0.9 g/dL (range, -3.8 to +5.3) compared with 2.8 g/dL (range, -2.2 to +7.5) for epoetin-treated patients (P < .0001).
During the study, 31.7% of placebo-treated patients achieved a > or = 2 g/dL HgB increase compared with 72.7% of epoetin-treated patients (P < .0001).
The incidence of toxicity in the groups was similar.
Changes in the average QOL scores from baseline to the end of the study were similar in the two groups (P = not significant).
Epoetin alfa significantly improved HgB and reduced transfusions in this patient population.
