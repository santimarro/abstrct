None of the patients in either group showed significant differences in liver function parameters between baseline and the last follow-up sample.
TDF was generally well tolerated and there were no severe treatment-related adverse events.
