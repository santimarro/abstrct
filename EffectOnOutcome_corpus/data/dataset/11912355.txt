This 1.9 mm Hg difference in intraocular-pressure reduction was significantly in favor of latanoprost (P < 0.001).
Ocular allergy (P < 0.001) and systemic side effects (P = 0.005) were reported significantly less frequently by latanoprost-treated patients compared with brimonidine-treated patients.
Latanoprost was better tolerated with less frequently occurring ocular allergy and systemic side effects.
