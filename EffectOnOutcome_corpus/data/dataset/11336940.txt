After 1 month of treatment, latanoprost significantly reduced intraocular pressure (mean +/- SEM) by 6.1 +/- 0.5 mm Hg (P <.001) and unoprostone by 4.2 +/- 0.4 mm Hg (P <.001) adjusted from an overall baseline of 22.3 +/- 0.5 mm Hg and 23.2 +/- 0.4 mm Hg, respectively.
Adverse ocular symptoms and findings were mild in both treatment groups.
Latanoprost once daily was significantly more effective in reducing intraocular pressure compared with unoprostone twice daily after 1 month of treatment in patients with primary open-angle glaucoma and ocular hypertension.
