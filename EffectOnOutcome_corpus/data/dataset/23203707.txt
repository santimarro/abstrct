Upon instillation, the global subjective ocular symptom score was significantly lower on T2345 than BPL on D42 (0.15 vs 0.41; p=0.001) and D84 (0.18 vs 0.46; p=0.001).
Moderate to severe conjunctival hyperaemia was less frequent on T2345 than on BPL at D42 (20.2% vs 30.6%; p=0.003) and D84 (21.4% vs 29.1%; p=0.02).
