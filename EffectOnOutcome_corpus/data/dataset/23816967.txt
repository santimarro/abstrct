More patients on afatinib (64%) versus chemotherapy (50%) experienced improvements in dyspnea scores (P = .010).
Compared with chemotherapy, afatinib significantly delayed the time to deterioration for cough (hazard ratio [HR], 0.60; 95% CI, 0.41 to 0.87; P = .007) and dyspnea (HR, 0.68; 95% CI, 0.50 to 0.93; P = .015), but not pain (HR, 0.83; 95% CI, 0.62 to 1.10; P = .19).
Afatinib showed significantly better mean scores over time in global health status/QoL (P = .015) and physical (P < .001), role (P = .004), and cognitive (P = .007) functioning compared with chemotherapy.
Fatigue and nausea were worse with chemotherapy, whereas diarrhea, dysphagia, and sore mouth were worse with afatinib (all P < .01).
In patients with lung adenocarcinoma with EGFR mutations, first-line afatinib was associated with better control of cough and dyspnea compared with chemotherapy, although diarrhea, dysphagia, and sore mouth were worse.
Global health status/QoL was also improved over time with afatinib compared with chemotherapy.
