Over time, levels of fatigue significantly decreased in all domains in all groups,
No significant differences in decline in fatigue were found between the PT+CBT and PT groups.
Physical training combined with cognitive-behavioral therapy and physical training alone had significant and beneficial effects on fatigue compared with no intervention.
