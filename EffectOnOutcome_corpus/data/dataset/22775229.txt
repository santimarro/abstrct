a 6-week treatment with LT or BT was equally effective in reducing IOP.
none of the administered drugs induced a significant effect on ocular blood flow parameters.
Both drugs were equally effective in reducing IOP (LT: -35.0% ±10.0%; BT: -33.6% ±8.8%, P=0.463 between groups).
no difference in ocular perfusion pressure was observed between the 2 treatment groups (P=0.1, between groups).
Neither LT nor BT altered ONHBF (P=0.4, baseline vs. treatment)
no effect on flow velocities in the retrobulbar vessels was seen with either of the 2 treatments.
