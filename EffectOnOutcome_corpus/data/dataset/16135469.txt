In analyses controlling for study site and baseline depressive symptoms, VID produced significant improvement in energy/fatigue at 6 months relative to CTL, particularly among women who felt less prepared for re-entry at baseline.
No significant main effect of the interventions emerged on cancer-specific distress,
but EDU prompted greater reduction in this outcome relative to CTL at 6 months for patients who felt more prepared for re-entry.
and no significant effects emerged on the secondary end points.
