We found no difference in severe ARSR between the groups at any point of assessment.
The patients reported low levels of skin related symptoms and no statistically significant differences between the groups were found.
No differences in ARSR between patients randomised to Calendula or Essex cream was found.
