Hormonal breast cancer treatment increases menopausal symptoms in women.
There were no significant differences in vaginal bleeding, mood alteration, or low energy.
Patients receiving tamoxifen had significantly more vaginal discharge (P < .0001).
Exemestane patients reported more bone/muscle aches (P < .0001), vaginal dryness (P = .0004), and difficulty sleeping (P = .03).
At 12 months, patients receiving tamoxifen had a significantly higher mean hot flash score (P = .03), with daily hot flashes increasing from baseline by 33% compared with a 7% increase from baseline with exemestane.
At 12 months, exemestane was associated with fewer hot flashes and less vaginal discharge than tamoxifen, but with more vaginal dryness, bone/muscle aches, and difficulty sleeping.
