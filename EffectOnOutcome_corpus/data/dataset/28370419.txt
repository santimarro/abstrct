TDF monotherapy was efficacious and safe for up to 144 weeks, providing an increasing rate of virologic response in heavily pretreated patients with multidrug-resistant HBV.
At week 144, the proportion with HBV DNA <15 IU/mL increased to 74.5%, which was significantly higher compared with that at week 48 (P = 0.03), without a significant difference between groups (P = 0.46).
No patients developed additional resistance mutations throughout the study period.
