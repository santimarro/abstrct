There were no significant differences between the narrow gastric tube group (NGT group, n=52) and the whole-stomach group (WS group, n=52) with regard to patient and cancer characteristics, operative procedure, postoperative intensive care unit (ICU) hospitalisation, and overall survival at 1 year.
Regarding the postoperative complication, there were more cases of postoperative reflux oesophagitis and impairment of pulmonary function in the WS group (P<0.05).
Regarding the QoL investigation, the scores of QoL dropped for all patients at 3 weeks after surgery.
Patients in the NGT group reported significantly (P<0.05) better scores of QoL at both 6 months and 1 year.
Patients who underwent gastric tube reconstruction develop less postoperative digestive tract complications, and have a quicker recovery and a better QoL during the follow-up period.
