Comparison of the population norm and clinical trial data showed that treatment with epoetin alfa resulted in clinically meaningful as well as statistically significant improvements in QOL (P <.01).
In the clinical trial, treatment with epoetin alfa overcame much of the QOL deficit seen in anemic cancer patients compared with the norm population sample.
