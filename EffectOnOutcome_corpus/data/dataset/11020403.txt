With a mean follow-up of 9.7 months, the trabeculectomy group had statistically lower intraocular pressures at weeks 6 to 15 (12.6 mm Hg vs 16.4 mm Hg) and months 11 to 13 (11.4 mm Hg vs 17.2 mm Hg) than the Ahmed implant group.
Compared with preoperative status, no statistically significant differences between groups were noted for visual acuity, visual field, lens status, and final anterior chamber depth.
the Ahmed implant group had a greater adjunctive medication requirement.
Lower mean intraocular pressures were noted for the trabeculectomy group.
All other results, including success (as defined in this study) and frequency of complications, were comparable between the two groups.
There was no statistically significant difference in the rate of complications between the two groups.
