Bevacizumab, a monoclonal antibody against vascular endothelial growth factor, increases survival when combined with irinotecan-based chemotherapy in first-line treatment of metastatic colorectal cancer (CRC).
hazard ratio was 0.42; P = .088.
Grade 3 hypertension was more common with bevacizumab treatment (16% v 3%)
Addition of bevacizumab to FU/LV as first-line therapy in CRC patients who were not considered optimal candidates for first-line irinotecan treatment provided clinically significant patient benefit, including statistically significant improvement in progression-free survival.
