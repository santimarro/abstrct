Peak oxygen consumption increased by 0.24 L/min in the exercise group,
Overall QOL increased by 9.1 points in the exercise group compared with 0.3 points in the control group (mean difference, 8.8 points; 95% CI, 3.6 to 14.0; P =.001).
Exercise training had beneficial effects on cardiopulmonary function and QOL in postmenopausal breast cancer survivors.
