Nocturnal administration of BCAA granules in patients with cirrhosis reduced the occurrence of muscle cramps in the leg but did not improve the patients' QOL.
There was no significant difference in any of the subjects revealed by the questionnaire regarding subjective or objective symptoms, or by the SF-8 between the daytime group and the nocturnal group after 3 months of treatment.
The daytime group showed a significant effect on general health, vitality, social functioning, mental health, and role emotional as revealed on the SF-8.
Conversely, the nocturnal group exhibited a significant decrease in the occurrence of muscle cramps in the legs (P = 0.014) and significantly improved Fisher's ratio after 3 months (P = 0.04).
