The global HRQOL scale was comparable at baseline on both treatment arms (P = .848);
Both treatments led to an improvement, over time, in dyspnoea.
