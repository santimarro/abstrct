Rates of normal alanine aminotransferase (ALT) and normalised ALT were similar between groups (p=0.41 and p=0.97 respectively).
Hepatitis B e antigen loss and seroconversion at week 240 were similar between groups, (p=0.41 and p=0.67 respectively).
No TDF resistance was observed up to week 240.
Treatment was generally well tolerated, and renal events were mild and infrequent (∼8.6%).
