The primary outcome was significantly lower in the intervention arm (adjusted difference -16.8, 95% CI -28.34 to -5.3; P = 0.006).
This intervention significantly reduced the unmet needs of cancer survivors and it is likely that it is cost-effective.
