Breast symptoms increased for both groups during radiotherapy, without loss of strength or range of movement.
The incidence of lymphedema during the study was low for both groups and did not differ between groups.
