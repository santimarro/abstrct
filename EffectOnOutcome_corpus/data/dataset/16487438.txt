After 6 months, none of the quality of life parameters in the low-TSH group was different from baseline values.
In the euthyroid group, motivation was significantly improved (Multidimensional Fatigue Index-20, P = 0.003),
No improvement in the SDQ score was observed.
In summary, quality of life in patients with DTC and long-term subclinical hyperthyroidism in general is preserved.
Restoration of euthyroidism in general does not affect quality of life.
