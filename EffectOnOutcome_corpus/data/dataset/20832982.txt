Patients in both treatment conditions showed improvements in pain, depression, QOL, and self-efficacy, and caregivers in both conditions showed improvements in anxiety and self-efficacy from baseline to four-month follow-up.
Results of exploratory analyses suggested that the CST intervention was more beneficial to patients/caregivers with Stage II and III cancers,
