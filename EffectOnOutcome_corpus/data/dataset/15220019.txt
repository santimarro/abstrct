Mean baseline diurnal IOP values were similar between the 2 groups.
Mean IOP values were similar at all time points except at 5:00 pm, when levels were significantly lower in latanoprost-treated patients (P = 0.025).
Fewer patients treated with latanoprost reported ocular or systemic AEs (P = 0.025 and P < 0.001, respectively).
