While in the hospital, patients assigned to LAC required fewer days of both parenteral analgesics compared with patients assigned to open colectomy (mean [median], 3.2 [3] vs 4.0 [4] days; P<.001) and oral analgesics (mean [median], 1.9 [1] vs 2.2 [2] days; P =.03).
Only minimal short-term QOL benefits were found with LAC for colon cancer compared with standard open colectomy.
