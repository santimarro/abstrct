however, many patients develop clinically relevant neurotoxicity, frequently resulting in treatment discontinuation
While myelosuppression was significantly more common in the combination, sequelae such as febrile neutropenia or infections were uncommon.
No statistically significant differences in quality of life scores between arms were noted.
Gemcitabine plus carboplatin significantly improves PFS and response rate without worsening quality of life for patients with platinum-sensitive recurrent ovarian cancer.
