We observed a significant reduction in anxiety (STAI, POMS) among group participants, a reduction in anger, depression and fatigue (POMS), a significant improvement in vigor and interpersonal relationships (POMS), in emotional and role functioning, in health status and fatigue level (EORTC QLQ-C30).
coping strategies (MAC) were not significantly different between groups.
No group-related negative effects were observed and the global satisfaction levels were very high.
