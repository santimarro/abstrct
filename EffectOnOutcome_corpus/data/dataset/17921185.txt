Clinically meaningful improvements in SF-36, pain, fatigue and sleep problems were also maintained throughout the 2 years of abatacept treatment.
No unique safety observations were reported during open-label exposure.
Improvements in the signs and symptoms of rheumatoid arthritis, physical function and health-related quality of life observed after 6 months, were maintained throughout the 2 years in this population with difficult-to-treat disease.
