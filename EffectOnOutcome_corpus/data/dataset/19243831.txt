Combined phacotrabeculectomy resulted in lower mean postoperative IOP than phacoemulsification alone at 3 months (14.0 vs 17.0 mmHg, P = 0.01), 15 months (13.2 vs 15.4 mmHg, P = 0.02), and 18 months (13.6 vs 15.9 mmHg, P = 0.01).
Combined phacotrabeculectomy resulted in 1.25 fewer topical glaucoma drugs (P<0.001) in the 24-month postoperative period, compared with phacoemulsification alone.
Combined surgery was associated with more postoperative complications (P<0.001) and more progression of optic neuropathy (P = 0.03), compared with phacoemulsification alone.
Combined phacotrabeculectomy is associated with more postoperative complications.
