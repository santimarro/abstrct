RT was then associated with increased breast symptoms and with greater (self-reported) fatigue, but with lower levels of insomnia and endocrine side effects.
No significant difference was found in the overall quality of life measure, with the no RT group having 0.36 units greater quality of life than the RT group (95% CI -5.09 to 5.81).
