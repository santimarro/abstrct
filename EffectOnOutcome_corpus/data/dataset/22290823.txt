Significant group by time interactions showed that there was an improvement in dyads' coping (p < 0.05), self-efficacy (p < 0.05), and social QOL (p < 0.01) and in caregivers' emotional QOL (p < 0.05).
Risk for distress accounted for very few moderation effects.
Both brief and extensive programs had positive outcomes for patient-caregiver dyads,
but few sustained effects.
Patient-caregiver dyads benefit when viewed as the 'unit of care'.
