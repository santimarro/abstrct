There was a significantly higher risk of death in the TMX120 group compared with the P group (hazard ratio, 1.39; 95% confidence interval, 1.07-1.81).
TMX does not prolong survival in patients with inoperable HCC and has an increasingly negative impact with increasing dose.
No appreciable advantage to QoL with TMX was observed.
