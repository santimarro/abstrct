No significant difference in patient benefit and QOL (Functional Assessment of Cancer Therapy-Lung).
No unexpected adverse events were observed.
Despite higher rates of some adverse effects (anemia, abdominal pain, constipation, fatigue)
