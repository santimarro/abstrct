There was no significant difference in HRQoL between nurse-led telephone and hospital follow-up at 12 months after treatment (p = 0.42; 95% confidence interval (CI) for difference: -1.93-4.64) and neither between follow-up with or without EGP (p = 0.86; 95% CI for difference: -3.59-3.00).
no differences between the intervention groups and their corresponding control groups were found in role and emotional functioning, and feelings of control and anxiety (all p-values > 0.05).
Replacement of most hospital follow-up visits in the first year after breast cancer treatment by nurse-led telephone follow-up does not impede patient outcomes.
nurse-led telephone follow-up seems an appropriate way to reduce clinic visits and represents an accepted alternative strategy.
