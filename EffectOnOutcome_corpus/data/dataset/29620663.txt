At the end of the study, the mean systolic blood pressure (SBP) of the ISH LSSalt group had significantly decreased by 10.18 mm Hg (95% confidence interval (CI): 3.13 to 17.2, P = .006) compared with that of the ISH NSalt group,
The mean diastolic blood pressure (DBP) had no significant differences in the ISH and NISH groups.
No obvious renin angiotensin system activation was found after LSSalt intervention.
The present study showed that the SBP of ISH patients was significantly decreased with the LSSalt intervention,
neither the SBP of the NISH patients nor the DBP of either group were similarly decreased, which indicated that ISH patients were more sensitive to salt restriction.
Regarding the urinary excretion of electrolytes and blood biochemical assays, the LSSalt treatment had the same effects on the ISH group as on the NISH group.
