In Group 1 and 2, intraocular pressure was well controlled to approximately 14 mmHg (9.38-18.46% decrease).
Generally, subjective satisfaction was improved after changing from PT to NPT (p = 0.03) and TBUT using PT was numerically inferior to that using NPT (p = 0.06) but not when changing from NPT to PT.
Both preservative containing and preservative-free 0.0015% tafluprost reduced intraocular pressure significantly.
changing medication from PT to NPT might improve subjective satisfaction and tear break up time.
