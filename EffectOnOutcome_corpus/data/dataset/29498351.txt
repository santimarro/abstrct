There was a greater reduction in WC, SBP, DBP and LAP index after MD compared with CD.
HDL:LDL ratio increased and total cholesterol, LDL-cholesterol, SBP, DBP and LAP index decreased only in MD.
The consumption of approximately 1200 mg of Ca/d (700 mg from fat-free milk+500mg from other dietary sources) associated with an energy-restricted diet decreased some of the MetS components and cardiometabolic measures in adults with T2DM.
