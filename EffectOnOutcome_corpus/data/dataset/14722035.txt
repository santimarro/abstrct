There was no significant difference in RR, RD, or PFS between arms.
DPPE plus DOX was statistically superior to DOX in OS (hazard ratio, 0.66; 95% CI, 0.48 to 0.91; P =.021).
No consistent influence on QOL was detected.
This study demonstrated no advantage in RR, RD, or PFS but significantly superior OS for DPPE plus DOX.
