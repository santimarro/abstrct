Compared with the spironolactone group (n=95), the clonidine group (n=92) presented similar rates of achieving the primary end point (20.5% versus 20.8%, respectively; relative risk, 1.01 [0.55-1.88]; P=1.00).
Secondary end point analysis showed similar office BP (33.3% versus 29.3%) and ambulatory BP monitoring (44% versus 46.2%) control for spironolactone and clonidine, respectively.
spironolactone promoted greater decrease in 24-h systolic and diastolic BP and diastolic daytime ambulatory BP than clonidine.
