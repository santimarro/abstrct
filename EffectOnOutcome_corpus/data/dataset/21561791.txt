Patients with PSA nadir ≤ 0.2 ng/ml, though had significantly higher 2-year risk of progression compared to CAD (53% vs. 31% (P = 0.03), respectively.
Patients without pain had a significantly lower 2-year risk of progression in both groups.
No QOL difference was found, although more side effects occurred in CAD.
Metastatic prostate cancer patients with high baseline PSA, pain, and high PSA nadir have a poor prognosis with ADT.
Patients with low PSA nadir do significantly worse with IAD compared with CAD.
Without ADT testosterone remained at castrate level for 4 months.
