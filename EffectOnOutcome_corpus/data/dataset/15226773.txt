Malnutrition occurs frequently in patients with cancer of the gastrointestinal (GI) or head and neck area and can lead to negative outcomes.
The NI group had statistically smaller deteriorations in weight (P<0.001), nutritional status (P=0.020) and global QoL (P=0.009) compared with those receiving UC.
