Compared to EUC, TEC was associated with increased pain communication self-efficacy after the intervention (P<.001);
both groups showed significant (P<.0001), similar, reductions in pain misconceptions.
At 2 weeks, assignment to TEC was associated with improvement in pain-related impairment (-0.25 points on a 5-point scale, 95% confidence interval -0.43 to -0.06, P=.01)
but not in pain severity (-0.21 points on an 11-point scale, -0.60 to 0.17, P=.27).
There were no significant intervention by subgroup interactions (P>.10).
We conclude that TEC, compared with EUC, resulted in improved pain communication self-efficacy and temporary improvement in pain-related impairment, but no improvement in pain severity.
