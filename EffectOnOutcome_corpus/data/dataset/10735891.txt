Overall mean reductions in combined pain and opioid analgesic intake were greater for suramin plus HC (rank sum P =.0001).
Pain response was achieved in a higher proportion of patients receiving suramin than placebo (43% v 28%; P =.001),
duration of response was longer for suramin responders (median, 240 v 69 days; P =.0027).
Time to disease progression was longer (relative risk = 1.5; 95% confidence interval, 1.2 to 1.9)
the proportion of patients with a greater than 50% decline in PSA was higher (33% v 16%; P =.01) in patients who received suramin.
Neither quality of life nor performance status was decreased by suramin treatment,
overall survival was similar.
